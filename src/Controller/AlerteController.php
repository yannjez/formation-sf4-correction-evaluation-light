<?php

namespace App\Controller;

use App\Entity\Alerte;
use App\Form\AlerteType;
use App\Repository\AlerteRepository;
use App\Repository\AlerteTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AlerteController extends AbstractController
{
    /**
     * @Route("/alerte/{type}", name="alerte_index", defaults={"type"=""})
     */
    public function index($type , AlerteRepository $alerteRepository,AlerteTypeRepository $alerteTypeRepository)
    {
        $viewData =[] ;
        $viewData['alertes'] = $alerteRepository->getNonResolved($type);
        $viewData['alerteTypes'] = $alerteTypeRepository->findAll();
        return $this->render('alerte/index.html.twig',$viewData);
    }

    /**
     * @Route("/alerte/detail/{id}", name="alerte_detail")
     */
    public function detail($id, AlerteRepository $alerteRepository,FormFactoryInterface $formFactory,Request $request,UrlGeneratorInterface $urlGenerator)
    {

        $alerte = $alerteRepository->find($id);

        $builder = $formFactory->createBuilder(AlerteType::class,$alerte);
        $builder->add('submit', SubmitType::class, [
            'label' => 'Sauver',
            'attr' => ['class' => 'btn btn-default pull-right']]);

        /* supprimer un champ */
        $builder->remove('date');
        $builder->remove('client');
        $builder->remove('type');
        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if($form->isValid()){
                $this->getDoctrine()->getManagerForClass(Alerte::class)->flush();
                $url = $urlGenerator->generate('alerte_detail',['id'=>$id]);
                return new RedirectResponse($url);
            }
        }
        $viewData =[] ;
        $viewData['form']= $form->createView();
        $viewData['alerte'] = $alerte;
        return $this->render('alerte/detail.html.twig',$viewData);
    }
}
