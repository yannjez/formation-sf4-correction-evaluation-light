<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Repository\AlerteRepository;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ClientController extends AbstractController
{
    /**
     * @Route("/", name="client_index")
     */
    public function index(ClientRepository $clientRepo)
    {
        $viewData = [];
        $viewData['clients']= $clientRepo->findAll();
        return $this->render('client/index.html.twig',$viewData );
    }

    /**
     * @Route("/client/{id}", name="client_detailClient")
     */
    public function detailClient(ClientRepository $clientRepo,$id,AlerteRepository $alerteRepository)
    {
        $viewData = [];
        $viewData['client']= $clientRepo->find($id);
        $viewData['alertes']= $alerteRepository->findBy(['client'=>$id]);
        return $this->render('client/detail_client.html.twig',$viewData );
    }

    /**
     * @Route("/client/create", name="client_create")
     */
    public function create(FormFactoryInterface $formFactory,Request $request,UrlGeneratorInterface $urlGenerator)
    {

        $client= new Client();
        $builder = $formFactory->createBuilder(ClientType::class,$client);
        $builder->add('submit', SubmitType::class, [
            'label' => 'Sauver',
            'attr' => ['class' => 'btn btn-default pull-right']]);

        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if($form->isValid()){
                $this->getDoctrine()->getManagerForClass(Client::class)->flush();
                $url = $urlGenerator->generate('client_index');
                return new RedirectResponse($url);
            }
        }
        $viewData =[] ;
        $viewData['form']= $form->createView();
        return $this->render('client/create.html.twig',$viewData);
    }


}
