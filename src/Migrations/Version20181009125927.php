<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181009125927 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_3AE753AC54C8C93');
        $this->addSql('DROP INDEX IDX_3AE753A19EB6921');
        $this->addSql('CREATE TEMPORARY TABLE __temp__alerte AS SELECT id, client_id, type_id, date, date_process FROM alerte');
        $this->addSql('DROP TABLE alerte');
        $this->addSql('CREATE TABLE alerte (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, client_id INTEGER DEFAULT NULL, type_id INTEGER DEFAULT NULL, date DATETIME NOT NULL, date_process DATETIME DEFAULT NULL, CONSTRAINT FK_3AE753A19EB6921 FOREIGN KEY (client_id) REFERENCES client (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_3AE753AC54C8C93 FOREIGN KEY (type_id) REFERENCES alerte_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO alerte (id, client_id, type_id, date, date_process) SELECT id, client_id, type_id, date, date_process FROM __temp__alerte');
        $this->addSql('DROP TABLE __temp__alerte');
        $this->addSql('CREATE INDEX IDX_3AE753AC54C8C93 ON alerte (type_id)');
        $this->addSql('CREATE INDEX IDX_3AE753A19EB6921 ON alerte (client_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_3AE753A19EB6921');
        $this->addSql('DROP INDEX IDX_3AE753AC54C8C93');
        $this->addSql('CREATE TEMPORARY TABLE __temp__alerte AS SELECT id, client_id, type_id, date, date_process FROM alerte');
        $this->addSql('DROP TABLE alerte');
        $this->addSql('CREATE TABLE alerte (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, client_id INTEGER DEFAULT NULL, type_id INTEGER DEFAULT NULL, date DATETIME NOT NULL, date_process DATETIME NOT NULL)');
        $this->addSql('INSERT INTO alerte (id, client_id, type_id, date, date_process) SELECT id, client_id, type_id, date, date_process FROM __temp__alerte');
        $this->addSql('DROP TABLE __temp__alerte');
        $this->addSql('CREATE INDEX IDX_3AE753A19EB6921 ON alerte (client_id)');
        $this->addSql('CREATE INDEX IDX_3AE753AC54C8C93 ON alerte (type_id)');
    }
}
