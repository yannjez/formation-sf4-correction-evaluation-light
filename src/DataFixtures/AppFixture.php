<?php

namespace App\DataFixtures;

use App\Entity\Alerte;
use App\Entity\AlerteType;
use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $faker = \Faker\Factory::create('fr_FR');
        $types =[];


        for($i= 0 ; $i < 3 ; $i++){
            $item = new AlerteType();
            $item->setNom($faker->userName);
            $types[] = $item;
            $manager->persist($item);

        }
        for($i= 0 ; $i < 5 ; $i++){
            $item = new Client();
            $item->setNom($faker->company);
            $item->setCodePostal($faker->numerify('#####'));
            $item->setEmail($faker->companyEmail);
            $item->setVille($faker->city);
            $item->setTelephone($faker->phoneNumber);

            for($j = 0 ; $j< 4 ; $j++){
                shuffle($types);
                $al = new Alerte();
                $al->setClient($item);
                $al->setType($types[0]);
                $al->setDate($faker->dateTimeThisYear);
                if( $j%3 == 0){
                    $al->setDateProcess($faker->dateTimeThisYear);
                }
                $manager->persist($al);
            }
            $manager->persist($item);
        }
        $manager->flush();
    }
}
