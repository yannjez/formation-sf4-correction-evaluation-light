<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codePostal;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telephone;
    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $ville;

    /**
     * @return mixed
     */
    public function getVille() {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville): void {
        $this->ville = $ville;
    }
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;




    public function getId(): ?int
    {
        return $this->id;
    }



    /**
     * @return mixed
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getCodePostal() {
        return $this->codePostal;
    }

    /**
     * @param mixed $codePostal
     */
    public function setCodePostal($codePostal): void {
        $this->codePostal = $codePostal;
    }

    /**
     * @return mixed
     */
    public function getTelephone() {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone): void {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void {
        $this->email = $email;
    }



    /*
     * 3.1 Client
Id
Nom
Code Postal  (non obligatoire )
Ville (non obligatoire )
Téléphone  (non obligatoire )
Email (non obligatoire  / valider le format EMAIL)
3.3 Alertes
Id
Date prévue
Date d'exécution ( un action non effectuée à une date d'exécution nulle )
Client ( => Client) ( obligatoire)
Type( => Type d’alerte) ( obligatoire)
3.4 Type d’alerte
Id
Nom

     */
}
