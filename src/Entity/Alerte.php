<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlerteRepository")
 */
class Alerte
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /*
     * Id
Date prévue
Date d'exécution ( un action non effectuée à une date d'exécution nulle )
Client ( => Client) ( obligatoire)
Type( => Type d’alerte) ( obligatoire)
     */

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date ;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateProcess ;

    /**
     * @return mixed
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDateProcess() {
        return $this->dateProcess;
    }

    /**
     * @param mixed $dateProcess
     */
    public function setDateProcess($dateProcess): void {
        $this->dateProcess = $dateProcess;
    }

    /**
     * @return mixed
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client): void {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void {
        $this->type = $type;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     */
    private $client ;
    /**
     * @ORM\ManyToOne(targetEntity="AlerteType")
     */
    private $type ;






    public function getId(): ?int
    {
        return $this->id;
    }
}
