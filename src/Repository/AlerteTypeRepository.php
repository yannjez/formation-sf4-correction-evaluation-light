<?php

namespace App\Repository;

use App\Entity\AlerteType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AlerteType|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlerteType|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlerteType[]    findAll()
 * @method AlerteType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlerteTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AlerteType::class);
    }

//    /**
//     * @return AlerteType[] Returns an array of AlerteType objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AlerteType
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
